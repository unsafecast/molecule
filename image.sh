#!/bin/sh

try() {
    ret=$?
    if [ $ret -ne 0 ]; then
        echo "previous command didn't exit with 0"
        exit $ret
    fi
}

IMAGE="build/image.img"
MOUNTPOINT="build/mountpoint_image"

mkdir -p $MOUNTPOINT
try

rm -f $IMAGE
try

echo ":: Creating image"
dd if=/dev/zero of=$IMAGE count=10 bs=1M
try

echo ":: Formatting Image"
parted -s $IMAGE mklabel gpt
try
parted -s $IMAGE mkpart primary 2048s 100%
try

echfs-utils -g -p0 $IMAGE quick-format 512

echo ":: Copying files"
echfs-utils -g -p0 $IMAGE import build/limine.sys /limine.sys
echfs-utils -g -p0 $IMAGE import build/limine.cfg /limine.cfg
echfs-utils -g -p0 $IMAGE import build/kernel/kernel /kernel

echo ":: Installing Limine"
limine-install $IMAGE
try

