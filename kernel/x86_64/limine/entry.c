#include <kernel/stivale2/stv2.h>
#include <stdint.h>
#include <stddef.h>

static uint8_t KERNEL_STACK[8192];

static struct stivale2_header_tag_framebuffer FRAMEBUFFER_TAG = {
    .tag = {
        .identifier = STIVALE2_HEADER_TAG_FRAMEBUFFER_ID,
        .next = 0,
    },

    .framebuffer_width = 0,
    .framebuffer_height = 0,
    .framebuffer_bpp = 0,
};

__attribute__((section(".stivale2hdr"), used))
static struct stivale2_header STIVALE_HEADER = {
    .entry_point = 0,
    .stack = (uintptr_t)KERNEL_STACK + sizeof(KERNEL_STACK),
    .flags = (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4),
    .tags = (uintptr_t)&FRAMEBUFFER_TAG,
};

void* get_tag(struct stivale2_struct* stivale2, uint64_t id) {
    struct stivale2_tag* current_tag = (void*)stivale2->tags;

    while (1) {
        if (current_tag == 0) {
            return 0;
        }

        if (current_tag->identifier == id) {
            return current_tag;
        }

        current_tag = (void*)current_tag->next;
    }

    return 0;
}

void _start(struct stivale2_struct* stivale2) {
    struct stivale2_struct_tag_framebuffer* fb_tag = get_tag(stivale2, STIVALE2_STRUCT_TAG_FRAMEBUFFER_ID);

    if (fb_tag == 0) {
        while (1) asm ("hlt");
    }

    uint32_t* fb = fb_tag->framebuffer_addr;

    // TODO: Remove this
    for (size_t y = 0; y < fb_tag->framebuffer_height / 2; y++) {
        for (size_t x = 0; x < fb_tag->framebuffer_width; x++) {
            fb[y * fb_tag->framebuffer_width + x] = 0xffffff;
        }
    }
    while (1) asm ("hlt");
}

