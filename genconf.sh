#!/bin/sh

mkdir -p build

const() {
    echo "$1 = $2" >> build/conf.ninja
}

ARCH=x86_64
BOOTLOADER=limine

ROOT=$(pwd)
BUILD=$ROOT/build

MODE="debug"

GENERIC_CFLAGS="-Wall -Wextra -ffreestanding -I$ROOT"
CC="clang"
LD="ld -static -nostdlib"

const "arch" "$ARCH"
const "bootloader" "$BOOTLOADER"
const "root" "$ROOT"
const "builddir" "$BUILD"

if test "$MODE" = "debug"; then
    const "cflags" "$GENERIC_CFLAGS -g -O0"
elif test "$MODE" = "release"; then
    const "cflags" "$GENERIC_CFLAGS -O3"
else
    >&2 echo "error: build mode doesn't match anything"
fi

const "cc" "$CC"
const "ld" "$LD"

